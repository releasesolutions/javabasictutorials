package br.com.releasesolutions;

import java.util.Random;

public class RandomNumber {

    private Random dice;

    public RandomNumber(){
    }

    public RandomNumber(Random dice) {
        this.dice = new Random();
    }

    public Random getDice() {
        return dice;
    }

    public void setDice(Random dice) {
        this.dice = dice;
    }

    public void printDices(){
        int num;

        for (int count = 1; count < 11; count++){
            num = getDice().nextInt(50);
            System.out.println(num);
        }
    }
}
