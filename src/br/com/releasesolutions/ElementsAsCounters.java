package br.com.releasesolutions;

import java.util.Random;

public class ElementsAsCounters {

    private Random random;
    private int[] freq;

    public ElementsAsCounters() {
    }

    public ElementsAsCounters(int freq) {
        this.freq = new int[11];
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public int[] getFreq() {
        return freq;
    }

    public void setFreq(int[] freq) {
        this.freq = freq;
    }

    public void printFrequencies() {

        for (int roll = 1; roll < 21; roll++) {
            ++freq[1 + random.nextInt(10)];
        }

        System.out.println("Face\tFrequency: ");
        for (int face = 0; face < getFreq().length; face++) {
            System.out.println(face + "\t" + freq[face]);
        }
    }
}
