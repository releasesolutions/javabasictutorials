package br.com.releasesolutions;

public class Poupanca {

    private double poupanca;
    private double investimento;
    private double juros;
    private double limiteDesejavel;

    public Poupanca() {
    }

    public double getPoupanca() {
        return poupanca;
    }

    public void setPoupanca(double poupanca) {
        this.poupanca = poupanca;
    }

    public double getInvestimento() {
        return investimento;
    }

    public void setInvestimento(double investimento) {
        this.investimento = investimento;
    }

    public double getJuros() {
        return juros;
    }

    public void setJuros(double juros) {
        this.juros = juros;
    }

    public double getLimiteDesejavel() {
        return limiteDesejavel;
    }

    public void setLimiteDesejavel(double limiteDesejavel) {
        this.limiteDesejavel = limiteDesejavel;
    }

    public void youShouldSafeMoney(){

        int mes = 1;

        while (poupanca < limiteDesejavel){
            System.out.println("Continue a poupar");
            poupanca = investimento * Math.pow(1 + juros, mes);
            System.out.printf(mes + "\t\t" + "%2.2f\n", poupanca);

            if (poupanca >= limiteDesejavel)
                System.out.println("Parab�ns! Voc� atingiu seu objetivo.");

            mes++;
        }
    }

    @Override
    public String toString() {
        return "Poupan�a: " + poupanca + "\tInvestimento: " + investimento + "\tJuros: " + juros + "\tLimite desejavel: " + limiteDesejavel;
    }
}
