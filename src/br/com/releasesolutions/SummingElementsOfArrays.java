package br.com.releasesolutions;

import java.util.Arrays;

public class SummingElementsOfArrays {

    private int[] numbers = {1, 2, 3, 4};
    private int sum;
    private int mult = 1;

    SummingElementsOfArrays() {
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    void sumOfElementsOfArray() {

        System.out.println(Arrays.toString(getNumbers()));
        for (int number : numbers) sum = sum + number;
        System.out.println(sum);
    }

    void multiplicationOfElementsOfArray() {
        System.out.println(Arrays.toString(getNumbers()));
        for (int number : numbers) mult = mult * number;
        System.out.println(mult);
    }
}
