package br.com.releasesolutions;

import java.util.Random;

public class MainApplication {

    public static void main(String[] args) {

        RandomNumber randomNumber1 = new RandomNumber();
        Random random = new Random();
        randomNumber1.setDice(random);
        randomNumber1.printDices();

        RandomNumber randomNumber2 = new RandomNumber(new Random());
        randomNumber2.printDices();

        SummingElementsOfArrays elements = new SummingElementsOfArrays();
        elements.sumOfElementsOfArray();

        elements.multiplicationOfElementsOfArray();

        RaizQuadrada raizQuadrada = new RaizQuadrada();
        raizQuadrada.setNumber1(25);
        raizQuadrada.setNumber2(49);

        System.out.println("Raiz number1: " + raizQuadrada.getRaizQuadrada(raizQuadrada.getNumber1()));
        System.out.println("Raiz number2: " + raizQuadrada.getRaizQuadrada(raizQuadrada.getNumber2()));

        System.out.println(raizQuadrada.toString());

        ElementsAsCounters elements1 = new ElementsAsCounters(11);
        elements1.setRandom(new Random());
        elements1.printFrequencies();

        System.out.println("\n");
        ElementsAsCounters elements2 = new ElementsAsCounters();
        elements2.setRandom(new Random());
        elements2.setFreq(new int[11]);
        elements2.printFrequencies();


        Poupanca poupanca1 = new Poupanca();
        poupanca1.setInvestimento(5000);
        poupanca1.setJuros(0.01);
        poupanca1.setPoupanca(0);
        poupanca1.setLimiteDesejavel(100000);

        poupanca1.youShouldSafeMoney();

    }
}
